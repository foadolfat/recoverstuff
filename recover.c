#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

//the below sequence debugs code and tells us where the dumped core occured
//clang -g recover.c -o recover
//./recover
//gdb ./recover core
//these are changes made for the purpose of testing git
/*
folfat:~/environment $ cd RecoverStuff
folfat:~/environment/RecoverStuff $ git init
Initialized empty Git repository in /home/ec2-user/environment/RecoverStuff/.git/
folfat:~/environment/RecoverStuff (master) $ git config --global user.name "Foad Olfat"
folfat:~/environment/RecoverStuff (master) $ git config --global user.email olfatfoad@gmail.com
folfat:~/environment/RecoverStuff (master) $ ls
core  recover  recover.c
folfat:~/environment/RecoverStuff (master) $ git add recover.c
folfat:~/environment/RecoverStuff (master) $ git commit -m "First commit"
[master (root-commit) 95146f1] First commit
 1 file changed, 119 insertions(+)
 create mode 100644 recover.c
folfat:~/environment/RecoverStuff (master) $ 

The above are how we used git here


This is another cnange to test git
*/

// Function prototypes. Don't change these.
unsigned char * read_card(char * fname, int *size);
void save_jpeg(unsigned char * data, int size, char * fname);
void recover(unsigned char * data, int size);

int main()
{
    // Read the card.raw file into an array of bytes (unsigned char)
    int card_length;
    unsigned char *card = read_card("card.raw", &card_length);
    
    // Recover the images
    recover(card, card_length);
}

unsigned char * read_card(char * fname, int * size)
{

    struct stat st;
    if (stat(fname, &st) == -1)
    {
        fprintf(stderr, "Can't get info about %s\n", fname);
        exit(1);
    }
    int len = st.st_size;
    unsigned char *raw = (unsigned char *)malloc(len * sizeof(unsigned char));
    
    FILE *fp = fopen(fname, "rb");
    if (!fp)
    {
        fprintf(stderr, "Can't open %s for reading\n", fname);
        exit(1);
    }
    
    char buf[512];
    int r = 0;
    while (fread(buf, 1, 512, fp))
    {
        for (int i = 0; i < 512; i++)
        {
            raw[r] = buf[i];
            r++;
        }
    }
    fclose(fp);
    
    *size = len;
    return raw;
}

void save_jpeg(unsigned char * data, int size, char * fname)
{
    FILE *fp = fopen(fname, "wb");
    if (!fp)
    {
        fprintf(stderr, "Can't write to %s\n", fname);
        exit(1);
    }
    
    fwrite(data, 1, size, fp);
    fclose(fp);   
}

void recover(unsigned char *card, int card_length){
    int numOfPics = 0;
    long int picStartLocs[100]; //when using *int *picStartLocs; it dumped core due to segmentation fault, so using a sized array instead
    for(long int i=0; i<card_length; i+=512){//Find picture beginings/total number of pics
        if((card[i] == 0xff && card[i+1] == 0xd8 && card[i+2] == 0xff) && (card[i+3] == 0xe0 || card[i+3] == 0xe1)){
            picStartLocs[numOfPics] = i;
            numOfPics++;
        }
    }
    printf ("%d\n", numOfPics);//print total number for testing
    
    long int picEndLoc[numOfPics];
    for(int k=1; k<numOfPics; k++){//find location of all end points aside from the very last one
        for(long int b=picStartLocs[k]; b>0; b--){
            if(card[b] == 0xd9 && card[b-1] == 0xff){
                picEndLoc[k-1] = b;
                break;
            }
        }
    }
    for(long int q=card_length; q>0; q--){//find location of the very last end point
        if(card[q] == 0xd9 && card[q-1] == 0xff){
            picEndLoc[28] = q;
            break;
        }
    }

    int sizeOfPic[numOfPics];
    
    for(int h=0; h<numOfPics; h++){//assign size of each pic based on its start and end location
        sizeOfPic[h] = picEndLoc[h] - picStartLocs[h];
    }
    for(int g=0; g<29; g++){//call save_jpeg for each picture
        
        unsigned char picture[sizeOfPic[g]];
        
        for(int m=0; m<sizeOfPic[g]; m++){

            picture[m] = card[picStartLocs[g]+m];
            
        }
        printf("Picture %d starts at %ld, ends at %ld and its size is %d\n\n", g+1, picStartLocs[g], picEndLoc[g], sizeOfPic[g]);//printing each picture's data for testing
        char picName[5];
        sprintf(picName, "pic%d.jpg", g);
        save_jpeg(picture, sizeOfPic[g], picName);
        
    }
    
    
}